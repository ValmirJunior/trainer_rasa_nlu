echo 'splitting data....'
rasa data split nlu

echo 'training model....'
rasa train nlu -u train_test_split/training_data.yml

echo 'testing model....'
rasa test nlu --nlu train_test_split/test_data.yml
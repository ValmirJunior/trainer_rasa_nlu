import re

import pandas as pd


def is_number(s):
    number_regex = re.compile(
        r'^([\[\]\-()|/!º°¤.,;:$×*+_?\d/\s]|a|c|e|g|h|l|o|q|p|x|kg|mg|ml|ou|anos?|cep|dias?|pdf|\x01)+$',
        re.IGNORECASE
    )

    return s and bool(number_regex.match(s))


def getting_intents(path_file):
    df = pd.read_csv(path_file)

    # df = pd.read_csv('../data/patient_classified_sentences.csv')
    # df = pd.read_csv('../../data/conversations_sequential_classified.csv')
    # df = df.dropna()
    # df_filtered = df[df.apply(lambda x: not is_number(x['train_txt']), axis=1)]

    # patients_sentences = df_filtered[df_filtered['ori'] == 'patient'][
    #     ['train_txt', 'intent', 'ents', 'annotated_text']]  # .iloc[:1000]
    # patients_sentences['annotated_text'] = patients_sentences['annotated_text'].str.strip()
    # grouped = patients_sentences.groupby(['intent'])
    #
    # return grouped['annotated_text'].apply(list).to_dict()

    grouped = df.groupby(['intent'])

    return grouped['annotated_txt'].apply(list).to_dict()


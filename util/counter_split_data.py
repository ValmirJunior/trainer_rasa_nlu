import glob
import os

nlu_file = 'nlu.yml'
train_test_split_folder = 'train_test_split'
training_file = 'training_data.yml'
test_file = 'test_data.yml'

# result_models = glob.glob("/home/junior/dev/python/trainer_rasa_nlu/rasa/result_models/*")
result_models = glob.glob("/home/junior/dev/python/trainer_rasa_nlu/rasa/train_test_split/*")

print(f'{"Model".ljust(12)} | {"Train".ljust(5)} | {"Test".ljust(4)}')


def find_intents(file):
    return [l for l in open(file, 'r').readlines() if l.startswith('- intent')]


def find_examples(file):
    examples = [l for l in open(file, 'r').readlines() if l.strip().startswith('-')]

    intents = find_intents(file)

    return [e for e in examples if e not in intents]


for result_model in result_models:
    model = result_model.split('/')[-1]

    nlu_full_path = os.path.join(result_model, nlu_file)
    training_file_full_path = os.path.join(result_model, train_test_split_folder, training_file)
    test_file_full_path = os.path.join(result_model, train_test_split_folder, test_file)

    count_nlu = find_examples(nlu_full_path)
    count_train = find_examples(training_file_full_path)
    count_test = find_examples(test_file_full_path)

    print(f'{model.ljust(12)} | {len(count_train)} | {len(count_test)}')
    # print(f'{model.ljust(12)} | {len(count_train)} | {len(count_test)} | {len(count_nlu)}')

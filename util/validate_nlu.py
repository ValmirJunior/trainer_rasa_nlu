
path_original = "/home/junior/dev/python/trainer_rasa_nlu/rasa/data/nlu.yml"
path_fixed = "/home/junior/dev/python/trainer_rasa_nlu/rasa/data/nlu.ymlfixed"


original_lines = open(path_original, 'r').readlines()
fixed_lines = open(path_fixed, 'r').readlines()

diferences=0

for i in range(140, 155):
    if(original_lines[i].strip() != fixed_lines[i].strip()):
        print('=========\n')
        print(i)
        print(original_lines[i:i+3])
        print('--------------------')
        print(fixed_lines[i:i+3])
        print('**********')
        diferences = diferences + 1


print(diferences)
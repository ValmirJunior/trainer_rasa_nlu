from process_data import getting_intents
# import yaml


# def make_nlu_data(path_file):
#     print('getting intents...')
#     data = getting_intents(path_file)
#
#     intents = [
#         {
#             "intent": key,
#             "examples": data[key]
#         }
#         for key in data.keys()
#     ]
#
#     return {
#         "version": "2.0",
#         "nlu": intents
#     }
#
#
# def save_nlu_data(path, data):
#     print(f'generating {path}')
#     dumped_data = yaml.dump(data, allow_unicode=True, sort_keys=False)
#
#     f = open(path, "w")
#     f.write(dumped_data)
#     f.close()
#     print('the content was saved in:', path)
#
# def fix_spacing(path):
#     nlu_file = open(path, 'r')
#     lines = nlu_file.readlines()
#     nlu_file.close()
#
#     last_index_start_example = None
#     in_example_block = False
#     for index, line in enumerate(lines):
#         if line.startswith('- intent:'):
#             in_example_block = False
#
#         elif line.strip() == 'examples:':
#             in_example_block = True
#             lines[index] = line[:-1] + ' |' + line[-1:]
#
#         elif in_example_block:
#             if lines[index].startswith('  -'):
#                 last_index_start_example = None
#                 lines[index] = '  ' + line
#             else:
#                 if not last_index_start_example:
#                     last_index_start_example = index - 1
#                 lines[last_index_start_example] += ' ' + line.strip()
#                 lines[last_index_start_example] = lines[index - 1].replace('\n', '') + '\n'
#                 lines[index] = ''
#
#     lines = list(filter(lambda x: x.strip(), lines))
#     lines = lines[:1] + ["\n"] + lines[1:]
#
#     nlu_file = open(path, 'w+')
#     nlu_file.write("".join(lines))
#     nlu_file.close()
#
#
# ## ----------------------------------------------
# ## old way
# nlu_path = "../rasa/data/nlu.yml"
#
# path_annotated_data = '../data/flair_pt_br/annotated_sentences.csv'
#
# save_nlu_data(nlu_path, make_nlu_data(path_annotated_data))
# fix_spacing(nlu_path)


##-----------------------------------------------
## New way


def generate_nlu_data(path_file):
    print('getting intents...')

    data = getting_intents(path_file)

    lines = [
        'version: \'2.0\'',
        '',
        'nlu:'
    ]

    for intent in data.keys():
        lines.append(f'- intent: {intent}')
        lines.append('  examples: |')

        for example in data[intent]:
            lines.append(f'    - {example}')

    return lines


def save_nlu_data(path_output_nlu, path_input_data):
    print(f'generating {path_output_nlu}')

    formatted_data = "\n".join(generate_nlu_data(path_input_data))

    f = open(path_output_nlu, "w+")
    f.write(formatted_data)
    f.close()
    print('the content was saved in:', path_output_nlu)


nlu_path = "../rasa/data/nlu.yml"

path_annotated_data = '../data/lasbe/annotated_sentences.csv'

save_nlu_data(nlu_path, path_annotated_data)
